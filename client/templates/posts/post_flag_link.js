Template.post_flag_link.events({
    'click #flagPost': function(e, template) {
        e.preventDefault();

        Meteor.call('flagPost' , this._id, this.title, this.userId, function(error, id) {
            if (error) {
                // display the error to the user
                throwError(error.reason);
            } else {
                Router.go('/');
            }
        });
    },
    'click #unFlagPost': function(e, template) {
        e.preventDefault();
        Meteor.call('unFlagPost' , this._id, function(error, id) {
            if (error) {
                // display the error to the user
                throwError(error.reason);
            } else {
                Router.go('/');
            }
        });
    }

});
