Template.post_page.helpers({
  isPending: function () {
    return this.post && this.post.status === Posts.config.STATUS_PENDING;
  },
  isGuest: function () {
    var user = Meteor.users.findOne();
    if (user) {
      return user.isGuest;
    }
  },
  isAdmin: function () {
    var user = Meteor.users.findOne();
    if (user) {
      return user.isAdmin;
    }
  }
});

Template.post_page.rendered = function(){
  $('body').scrollTop(0);
};
