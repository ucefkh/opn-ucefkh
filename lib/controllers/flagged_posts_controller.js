Posts.controllers.posts_flagged = Posts.controllers.list.extend({
    data: function () {
        return {
            terms: {
                view: 'posts_flagged',
                flagged:true,
                limit: 10
            }
        }
    }
});

Router.route('/flagged/:limit?', {
    controller: Telescope.controllers.posts_flagged,
    name: 'posts_flagged'
});