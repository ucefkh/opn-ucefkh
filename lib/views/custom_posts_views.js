Posts.views.add("posts_flagged", function (terms) {
    return {
        find: {flagged: terms.flagged},
        options: {limit: 10, sort: {createdAt: -1}},
        showFuture: true
    };
});



