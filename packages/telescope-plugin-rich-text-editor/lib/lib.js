/* Remove default post body */
Posts.removeField("body");

/* Add rich text editor version */
Posts.addField({
    fieldName: 'body',
    fieldSchema: {
        type: String,
        optional: true,
        //max: 8000,
        editableBy: ["member", "admin"],
        autoform: {
            row:6,
            afFieldInput: {
                type: 'froala',
                inlineMode: false,
                buttons: ['bold', 'italic' , 'underline', 'createLink' , 'insertImage', 'formatBlock','insertVideo'] // 'insertVideo'
            }
        }
    }
});

Comments.removeField("body");

/* Add rich text editor to comments*/
Comments.addField({
    fieldName: 'body',
    fieldSchema: {
        type: String,
        optional: true,
        //max: 8000,
        editableBy: ["member", "admin"],
        autoform: {
            row:6,
            afFieldInput: {
                type: 'froala',
                inlineMode: false,
                buttons: ['bold', 'italic' , 'underline', 'createLink' , 'insertImage', 'formatBlock'] // 'insertVideo'
            }
        }
    }
});



Telescope.schemas.userData.removeField("bio");

/* Add rich text editor to users bio */
Telescope.schemas.userData.addField({
    fieldName: 'bio',
    fieldSchema: {
        type: String,
        optional: true,
        //max: 8000,
        editableBy: ["member", "admin"],
        autoform: {
            row:6,
            afFieldInput: {
                type: 'froala',
                inlineMode: false,
                buttons: ['bold', 'italic' , 'underline', 'createLink' , 'insertImage', 'formatBlock'] // 'insertVideo'
            }
        }
    }
});